# CIter: faster iteration for GitLab Pipelines

> THIS PROJECT IS A PROOF OF CONCEPT, AND AS SUCH IS NOT OFFICIALLY SUPPORTED BY GITLAB

On a preconfigured repo, be able to run an arbitrary pipeline using:

## Goal

Iterate on a CI pipeline without needing to open Gitlab.com

## Usage

```bash
citer --help
citer --watch -v MY_VAR=1 -f test.txt my_pipeline.yaml
```

## Setup

1. Clone this repository
2. Install citer:
    ```bash
    cd /path/to/citer
    python3 setup.py install
    ```
3. Fork the base repository: https://gitlab.com/gitlab-org/incubation-engineering/mlops/poc/citer-base-ci   
4. **For the CI base repository**, generate necessary tokens:
   1. [Access token](https://gitlab.com/-/profile/personal_access_tokens)
   2. [Trigger token](https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token)
5. Export the necessary configurations:
    ```bash
    export CITER_TRIGGER_TOKEN=""
    export CITER_ACCESS_TOKEN=""
    export CITER_PROJECT_ID="Your project id, found under Settings > General"
    export CITER_REF="Reference can be a branch or a tag"
    ```

## Progress

- [x] Setup Repo
- [x] Uploads file using Issues API
- [x] Downloads CI and runs using Dynamic Parent/Child
- [x] Pass variables to CI
- [x] Watch CI execution from terminal
- [x] Pass additional files
- [ ] Show logs in case of error
- [ ] Show link to current job
- [ ] Custom gitlab urls
- [ ] Don't upload extra files if they have not been updated
- [ ] Upload directory as extra file
